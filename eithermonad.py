from monad import Monad


class EitherMonad(Monad):
    def __init__(self, value, left):
        super().__init__(value)
        self.left = left

    def fmap(self, f):
        if self.left:
            return self
        return EitherMonad(f(self.value), False)

    def apply(self, f):
        if self.left or f.left:
            return self
        return EitherMonad(f.value(self.value), False)

    def bind(self, f):
        if self.left:
            return self
        return f(self.value)

    def __str__(self):
        return str({True: "Left",
                    False: "Right"}[self.left]) + " " + str(self.value)


def Left(x):
    return EitherMonad(x, True)


def Right(x):
    return EitherMonad(x, False)


def isLeft(x):
    return x.left


def isRight(x):
    return not x.left


if __name__ == "__main__":
    x = Left(4)
    y = Right(4)
    print(x)
    print(y)
    print("\nFmap **2 to x and y")
    x = x.fmap(lambda x: x**2)
    y = y.fmap(lambda x: x**2)
    print(x, ",\texpected Left 4")
    print(y, ",\texpected Right 16")

    print(y >> (lambda x: Right(x*2))
            >> (lambda x: Left(x) if x < 20 else Right(x)))
