# libgenSearch
libgenSearch should become a little CLI-based tool
to query and download files from library genesis.

Queries should be made in a (rather small) SQL Subset, like:
- `select name,author,filetype where name="Algebraic Topology" and author="Allan Hatcher"`
- libgenSearch should be configurable by a config file
	- declare standart queries like "basic = Title,Author,Mirror1" : [None, None, 50]"
	- auto resolve download link
	- download path
	- postprocess command (a command which to invoce after finishing download)

# Make it work
- Implement parser for subset of sql select statement
- convert the where clauses to a dictionary and query filtered by that clauses
- select only the collumns defined by the select clause
- define a configuration structure
- depending on the configuration structure auto\_resolve the download links
- download by id into path stored in configuration structure
- postprocess file as defined by configuration

# Make it nice
- write a parser for the configuration file (or use json)

# Current Status:
See open issues and milestones
