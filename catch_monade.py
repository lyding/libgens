'''
This module contains the catch monad which is a mixture
of a maybe monade and an either monad in the way, that
if an operation should raise an exception the error
message is returned as a Left, otherwise the result is returned as right
'''
from monad import Monad


class CatchMonad(Monad):
    def __init__(self, value, left):
        super().__init__(value)
        self.left = left

    def fmap(self, f):
        if self.left:
            return self
        try:
            result = f(self.value)
        except Exception as e:
            return CatchMonad(str(e), True)
        return CatchMonad(result, False)

    def apply(self, f):
        if self.left or f.left:
            return self
        function = f.value
        try:
            result = function(self.value)
        except Exception as e:
            return CatchMonad(str(e), True)
        return CatchMonad(result, False)

    def bind(self, f):
        if self.left:
            return self
        try:
            result = f(self.value)
        except Exception as e:
            return CatchMonad(str(e), True)
        return CatchMonad(result, False)

    def __str__(self):
        return str({True: "CLeft:",
                    False: "CRight:"}[self.left]) + " " + str(self.value)


def CLeft(x):
    return CatchMonad(x, True)


def CRight(x):
    return CatchMonad(x, False)


def isCLeft(x):
    return x.left


def isCRight(x):
    return not x.left


def lift_to_catch(f):
    def inner(result):
        if isCLeft(result):
            return result
        return CRight(f(result.value), False)
    return inner


if __name__ == "__main__":
    x = (CRight(4000) >>
         (lambda x: x / 0) >>
         (lambda x: str(x)) >>
         (lambda x: x[2]) >>
         (lambda x: x * 10))
    if isCLeft(x):
        print("There was an error: ", str(x))
    else:
        print("Alles laeuft nach Wunsch, Dave")
