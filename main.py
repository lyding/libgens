from functools import reduce
import query_lang_parser
from eithermonad import isLeft
# from tabulate import tabulate


def tabulate_search_result(search_result: list) -> dict:
    '''
    The libgen_api search results are of the form
    result = [{col1: value, ..., coln: value}
                            ...
              {col1: value, ..., coln: value}]
    But the tabulate library expects a dictionary with the colum
    names as keys and a list of values
    This function transforms the search result in the expected format
    '''
    if len(search_result) == 0:
        return {}
    return reduce(lambda x, y: x | y,
                  list(map(lambda name: {
                      name: list(map(lambda x: x[name], search_result))},
                      search_result[0].keys())))


if __name__ == "__main__":
    _input = input('lgS ->> ')
    # print(query_lang_parser.atomic_clause_parser.parse(_input))
    # print(query_lang_parser.parse_collumn_name_collection.parse(_input))
    # print(query_lang_parser.parse_operand.parse(_input))
    # print(type(parse_integer.parse(_input).value[0]))
    # print(query_lang_parser.dnf_clause_parser.parse(_input))
    # print(query_lang_parser.by_statement_parser.parse(_input))
    query_statement = query_lang_parser.select_statement_parser.parse(_input)
    print(query_statement)
    if isLeft(query_statement):
        print(query_statement)
    else:
        result = query_lang_parser.query_server(query_statement.value[0])
        print(result.value)
