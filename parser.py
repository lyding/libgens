from monad import Monad
from eithermonad import Left, Right, isLeft, isRight
from functools import reduce
import operator


class Unit():
    def __init__(self):
        pass

    def __add__(self, other):
        return other

    def __radd__(self, other):
        return other

    def __or__(self, other):
        return other

    def __ror__(self, other):
        return other


class Parser(Monad):
    # {{{
    def __init__(self, value):
        # value :: (String -> Either String (a, String))
        super().__init__(value)

    def parse(self, args):
        return self.value(args)

    def fmap(self, f):
        parser_function = self.value

        def inner(xs):
            return f(parser_function(xs))
        return Parser(inner)

    def apply(self, f):
        raise NotImplementedError()

    def bind(self, f):
        def inner(xs):
            result = self.parse(xs)
            if isLeft(result):
                return result
            return f(result.value[0]).parse(result.value[1])
        return Parser(inner)

    def __str__(self):
        return str(self.value)

    def __or__(self, other):
        def inner(xs):
            resultA = self.parse(xs)
            if isLeft(resultA):
                resultB = other.parse(xs)
                if isLeft(resultB):
                    # Combine both error messages for the user to debug
                    return Left(resultA.value + "; " + resultB.value)
                return resultB
            return resultA
        return Parser(inner)

    def __add__(self, other):
        def inner(xs):
            resultA = self.parse(xs)
            if isLeft(resultA):
                return resultA
            resultB = other.parse(resultA.value[1])
            if isLeft(resultB):
                return resultB
            return Right((resultA.value[0] + resultB.value[0],
                          resultB.value[1]))
        return Parser(inner)

# }}}


def MZero(error):
    def inner(xs):
        return Left(error)
    return Parser(inner)


def MReturn(a):
    def inner(xs):
        return Right((a, xs))
    return Parser(inner)


def item():
    def inner(xs):
        if xs == "":
            return Right(("", ""))
        return Right((xs[0], xs[1:]))
    return Parser(inner)


def item1():
    def inner(xs):
        if xs == "":
            return Left("Failed parsing end of string")
        return Right((xs[0], xs[1:]))
    return Parser(inner)


def sat(predicate):
    return (item() >> (lambda c: MReturn(c) if predicate(c)
                       else MZero("predicate not fullfilled")))


def char(c, case_insensitive=False):
    if case_insensitive:
        return sat(lambda x: x.casefold() == c.casefold())
    return sat(lambda x: x == c)


def skipchar(c, case_insensitive=False):
    if case_insensitive:
        return (item() >> (lambda x: MReturn(Unit())
                           if x.casefold() == c.casefold()
                           else MZero("predicate not fullfilled")))
    return (item() >> (lambda x: MReturn(Unit()) if x == c
                       else MZero("predicate not fullfilled")))


# Could be implemented more readable with `chain`
def string(_string, case_insensitive=False):
    # Old cryptic code
    # if _string == "":
    #     return MReturn("")
    # return (char(_string[0])
    #         >> (lambda c: string(_string[1:])
    #         >> (lambda cs: MReturn(c + cs))))
    return chain(list(map(lambda s: char(s, case_insensitive), _string)))


def mute(parser, unit=Unit()):
    def inner(xs):
        result = parser.parse(xs)
        if isLeft(result):
            return result
        return Right((unit, result.value[1]))
    return Parser(inner)


def many(parser, op=operator.add, unit=Unit()):
    def inner(xs, old):
        result = parser.parse(xs)
        if isRight(result):
            parsed, rest = result.value
            return inner(rest, (Right((op(old.value[0], parsed), rest))))
        return old
    return Parser(lambda xs: inner(xs, Right((unit, xs))))


def many1(parser):
    return parser + many(parser)


def choice(parsers):
    return reduce(lambda x, y: x | y, parsers, MZero("No parser given"))


def chain(parsers):
    return reduce(lambda x, y: x + y, parsers)


def take_while(predicate, op=operator.add, unit=Unit()):
    def inner(xs, old):
        result = item1().parse(xs)
        if isLeft(result):
            return result
        parsed, rest = result.value
        if not predicate(parsed):
            return old
        return inner(rest, Right((op(old.value[0], parsed), rest)))
    return Parser(lambda xs: inner(xs, Right((unit, xs))))


def union(parserA, parserB, op=operator.add):
    def inner(xs):
        resultA = parserA.parse(xs)
        if isLeft(resultA):
            return resultA
        resultB = parserB.parse(resultA.value[1])
        if isLeft(resultB):
            return resultB
        return Right((op(resultA.value[0], resultB.value[0]),
                      resultB.value[1]))
    return Parser(inner)


if __name__ == "__main__":
    s = "Test"
    TorD = char("D") | char("T")
    skipspace = many(skipchar(' '))
    parseTs = many(char('T'))
    digit = choice(list(map(char, "1234567890")))

    print(item().parse(s))
    print(char("T").parse(s))
    print(string("Test").parse(s))
    print(TorD.parse(s))
    print(skipchar('T').parse(s))
    print(skipspace.parse("    " + s))
    print((char('T') + char('e')).parse(s))
    print(many1(char(' ')).parse(s), "expected Left")
    print(digit.parse("1234"))
    print(mute(string("123"), unit="").parse("123456"))
    print(take_while(lambda x: x != "4", unit="").parse("123456"))
