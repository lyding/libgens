"""
This module contains the parser of the sql-like query language
"""
from eithermonad import isLeft, Right
from parser import (many, many1, mute, skipchar, choice, string, char, chain,
                    take_while, Parser, MReturn, union)
from configuration import CONFIGURATION
from functools import reduce
from libgen_api import SearchRequest
from catch_monade import CatchMonad
import operator


# These are all the available collumn names. I got the from
# libgen_api.SearchRequest, they are unlikely to change.
# The second entry is the type corresponding stored in the collumns
# This information is important as integers and strings have to be parsed
# differently
col_names = {'id': "integer",
             'author': "string",
             'title': "string",
             'publisher': "string",
             'year': "string",
             'pages': "integer",
             'language': "string",
             'size': "string",
             'extension': "string",
             'mirror_1': "string",
             'mirror_2': "string",
             'mirror_3': "string",
             'mirror_4': "string",
             'edit': "string"}


# Skips over arbitrary many whitespaces
skipspace = many(skipchar(' '))


def skipstring(_str: str, case_insensitiv=False) -> Parser:
    """
    A small combined parser generator,
    the generated parser skips the specified string
    but fails if the string is not given.
    This is used to skip over required keywoards like 'and' or 'or'

    :param _str: The string to parse silently
    :returns: Parser Unit
    """
    return mute(string(_str, case_insensitiv))


def lift_to_parser_result(f):
    """
    Takes a function and lifts it to the domain of the parser result which
    is Either String (a, String).
    So lift_to_parser_result is of the form
    lift_to_parser_result: (a -> b) -> (Either String (b, string))

    If one wants to fmap a function of the form (a->b) over a parser
    the function has to be lifted beforehand
    """
    def inner(result):
        if isLeft(result):
            return result
        return Right((f(result.value[0]), result.value[1]))
    return inner


# Most commonly used functions already lifted to shorten the code
lifted_int = lift_to_parser_result(int)
lifted_list = lift_to_parser_result(lambda x: [x])
lifted_dict = lift_to_parser_result(dict)
lifted_casefold = lift_to_parser_result(lambda s: s.casefold())

# This parser parses either '*' or any string which is listed
# under 'predefined_selections'. This allows us to parse
# "select * " and "select ANY_PREDEFINED_NAME"
parse_predefined_selections = choice(
    list(map(
        string,
        ['*'] + CONFIGURATION['predefined_selections']))).fmap(lifted_list)

# Use the col_names to generate a parser which parses all the collumn names
# This atomic parser is used to build a more complex parser which parses
# a comma seperated list of collumn names
parse_collumn_name = choice(list(map(lambda s: string(s, True),
                                     col_names.keys()))).fmap(lifted_casefold)

# A select statement can include a collection of comma seperated col_names
# to query, so generate a parser for that as well
parse_collumn_name_collection = parse_collumn_name.fmap(lifted_list) +\
    many(skipspace +
         skipchar(',') +
         skipspace +
         parse_collumn_name.fmap(lifted_list))

# Atomic clauses are of the form COLL_NAME OPERAND VALUE
# like e.g. `Pages < 111` this parser will be used to
# parse the operand.
parse_operand = choice(list(map(string, [
    "=", "<>", "<=", "<", ">=", ">", "!="])))

# This parsers will be used to parse an arbitrary string, this is
# a sequence of items enclosed by quotation marks.
# BE AWARE of the fact that the take_while parser does not care for
# backslashes as an escape sequence, it will stop at the
# first quotation mark it encounters.
parse_string = skipchar('"') + take_while(lambda c: c != '"') + skipchar('"')
# The parse_integer parser will be used to parse strings which resembles
# digits and convert this string into integers. There is no risk
# of failing here. Either the parser succeded, than the parsed string
# consists only of digits and can be converted, or the parser failed in
# which case the failure is propagated anf the int function is not invoked
parse_digit = choice(list(map(char, "1234567890")))
parse_integer = many1(parse_digit).fmap(lifted_int)

# Now go through all items in the col_names dictionary and use the type
# information stored there to create a list of atomic clause parser
# each of these atomic parsers can parse a clause of the form
# COLLUMN_NAME OPERATOR [STRING, INTEGER]
# All three parsed values will be returned as a tuple of the form
# (opertor, collumn_name, value), so the resulting parser will have the type
# String -> Either String ((String, String, String), String)
atomic_clause_parser = choice(list(map(
    lambda entry:
        string(entry[0], True).fmap(lifted_casefold) >> (
        lambda collumn_name: (skipspace + parse_operand + skipspace) >> (
        lambda operator: {
            "string": parse_string,
            "integer": parse_integer}[entry[1]] >> (
        lambda value: MReturn((operator, collumn_name, value))))),
    col_names.items())))

# The where clause has to be in the disjunctive normal form
# that is the form (A and ... and C) or ... or (D and ... and E)
# To automatically get the bracketing correct the parser is build
# a sequentially. The conjunctive clause parser parses
# a conjunction of atomic clauses concatenated by the keyword 'and'
conjunctive_clause_parser = (
    atomic_clause_parser.fmap(lifted_list) +
    many(skipspace + skipstring("and", True) + skipspace +
         atomic_clause_parser.fmap(lifted_list))).fmap(lifted_list)
# While the final clause in disjunctive normal form parsed
# a sequence of conjunctive clauses concatenated by the keyword 'or'
dnf_clause_parser = conjunctive_clause_parser +\
    many(skipspace + skipstring("or", True) + skipspace +
         conjunctive_clause_parser)


# As the api only allows to query for books by one collum
# either author, title, ... it should be possible for the user to specify
# which collumns they query
by_statement_parser = chain([
    skipstring("by", True),
    skipspace,
    parse_collumn_name >> (
    lambda collum_name: skipspace +
    skipstring('=') +
    skipspace +
    {
        "string": parse_string,
        "integer": parse_integer
    }[col_names[collum_name]] >> (
    lambda parsed_result: MReturn((collum_name, parsed_result))))])


select_statement_parser = reduce(
    lambda a, b: union(a, b, operator.or_),
    [skipstring('select', True),
     skipspace,
     (parse_collumn_name_collection | parse_predefined_selections)
        .fmap(lift_to_parser_result(lambda x: {'selection': x})),
     skipspace,
     (by_statement_parser | MReturn(CONFIGURATION['default_by']))
        .fmap(lift_to_parser_result(lambda x: {'by': x})),
     skipspace,
     skipstring('where', True),
     skipspace,
     dnf_clause_parser.fmap(lift_to_parser_result(
         lambda x: {'where_clauses': x}))])


def query_server(query_statement):
    # The naive way would be to invoce the constructor and put the
    # initialise the catch monade with the result, but this way the
    # programme would crash if the constructor would raise an exception.
    # By placing a lambda function which invokes the constructor into
    # the catch monad as using a lambda runction to run the constructor
    # with map, even exceptions in the constructor will be handeld correctly
    # by the catch monad
    search_request = CatchMonad(lambda: SearchRequest(
        query_statement['by'][1], query_statement['by'][0]), False)
    search_request = search_request\
        .fmap(lambda x: x())\
        .fmap(lambda x: x.aggregate_request_data())
    return search_request
