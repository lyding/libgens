from abc import ABC, abstractmethod


class Monad(ABC):
    def __init__(self, value):
        self.value = value

    @abstractmethod
    def bind(self, f):
        pass

    @abstractmethod
    def fmap(self, f):
        pass

    @abstractmethod
    def apply(self, f):
        pass

    @abstractmethod
    def __str__(self):
        pass

    def __rshift__(self, other):
        return self.bind(other)
