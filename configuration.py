"""
This module contains the configuration structure
It will contain the parser logic as well
"""

CONFIGURATION = {
    'predefined_selections': [],
    'auto_resolve_download_links': False,
    'download_path': './',
    'postprocess_command': '',
    'default_by': 'title'}
